Pod::Spec.new do |s|
s.platform = :ios
s.ios.deployment_target = '10.0'
s.name         = "UBCabSpinner"
s.version      = "0.1.1"
s.summary      = "UBCab Activity Indicators"
s.requires_arc = true

s.swift_version = "5.0"
s.license = { :type => "MIT", :file => "LICENSE" }

s.author = { "Maidar Dashbayar" => "mdashbayr@gmail.com" }

s.homepage     = "https://gitlab.com/neelttei/ubc_spinner"
s.source = { :git => "https://gitlab.com/neelttei/ubc_spinner.git", :tag => "#{s.version}"}

s.framework = "UIKit"

s.source_files = "UBCabSpinner/*.{swift}", "UBCabSpinner/**/*.{swift}"

end
